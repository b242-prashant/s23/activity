let trainer = {
	"name" : "Ash Ketchum",
	Age : 10,
	Pokemon: ["Pikachu","Charizard","Squirtle","Bulbasaur"],
	Friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock","Misty"]
	}
}

trainer.talk = function(){
	console.log("Pikachu! I chose you!");
}

console.log(trainer.name);
console.log(trainer.Age);
console.log(trainer.Pokemon);
console.log(trainer.Friends);


console.log(trainer["name"]);
console.log(trainer['Age']);
console.log(trainer['Pokemon']);
console.log(trainer['Friends']);

trainer.talk();

function Pokemon(name, level){
	// Properties
	this.name = name;
	this.level = level;
	this.health = 2 * level;
	this.attack = level;
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name);
		target.health = target.health - this.attack;
		console.log(target.name+"'s health is now reduced to "+target.health);
		if(target.health<=0){
			this.faint(target);
		}
	};

	this.faint = function(target){
		console.log(target.name + ' fainted.');
	}

	
	
}

let pikachu = new Pokemon('Pikachu', 16);
console.log(pikachu);
let geodude = new Pokemon('Geodude', 8);
console.log(geodude);
let mewtwo = new Pokemon('Mewtwo',100)
console.log(mewtwo);


geodude.tackle(pikachu);
mewtwo.tackle(geodude);

